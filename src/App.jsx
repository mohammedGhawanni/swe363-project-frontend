import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter as Router } from "react-router-dom";
import LandingPage from './components/LandingPage';
import Route from 'react-router-dom/Route';
import Switch from 'react-router-dom/Switch';
import Register from './components/Register';
import Login from './components/Login';
import Profile from './components/Profile';
import Search from './components/Search';
import Header from './components/Header';
import Result from './components/Result';
import NotFound from './components/NotFound';
import AppliedRoute from "./components/AppliedRoute";


const ViewRoute = ({component : Component}) => (
    <Route
     render={props => (
         <div>
         <Header />
         <Component props={props} />
         //TODO: add a footer;
         </div>
     )}
     />
);


export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = { 
            isAuthenticated: false
        };
    }
    //This method will be used to set the authentication state of the user
    userHasAuthenticated = authenticated => {
        this.setState({ isAuthenticated: authenticated });
    }

    render() {
        //We will try to pass the reference of these properties to other components
        //That will require modifying the Route
        const childProps = {
            isAuthenticated: this.state.isAuthenticated,
            userHasAuthenticated: this.userHasAuthenticated
        };
        return (

            
            <div>
                <Router>
                    <Switch>
                        <AppliedRoute exact path="/" component={LandingPage} props={childProps}></AppliedRoute>
                        <AppliedRoute path="/login" component={Login} props={childProps} ></AppliedRoute>
                        <AppliedRoute path="/register" component={Register} props={childProps}></AppliedRoute>
                        <AppliedRoute path="/profile" component={Profile} props={childProps}></AppliedRoute>
                        <AppliedRoute path="/profile/:id" component={Profile} props={childProps}></AppliedRoute>
                        <AppliedRoute path='/search' component={Search} props={childProps}></AppliedRoute>
                        <AppliedRoute path='/header' component={Header} props={childProps}></AppliedRoute>
                        <AppliedRoute  component={NotFound}></AppliedRoute>
                        <AppliedRoute path='/results/:keyword' name='result' component={Result}></AppliedRoute>
                    </Switch>
                </Router>
            </div>
        )
    }
};