import React, { Component } from 'react';
import Header from './Header';
import axios from 'axios';
import '../styles/Register.css';
import Login from './Login';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email: '',
            password: '',
            passwordConfirm: '',
        }
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleClick(event) {
        event.preventDefault();
        if (!this.showFormErrors()) {
            console.log('form is invalid: do not submit');
        } else {
            console.log('form is valid: submit');
            var namePatter = "/"
            var payload = {
                "name": this.state.username,
                "email": this.state.email,
                "password": this.state.password
            }
            console.log(payload);
            var apiBaseURL = "http://localhost:3000/users/register";
            axios.post(apiBaseURL, payload, {
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((response) => {
                    console.log(response);
                    if (response.data.code == 200) {
                        console.log("Registration Successful!");
                    }
                    else if (response.data.code == 400) {
                        console.log("Registration Failed!");
                    }
                    else if (response.data.code == 404) {
                        console.log("Resgistration Not Found!");
                    }
                    else if (response.data.code == 401) {
                        console.log("Resgistration Not Authorized!");
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }
    handleChange(e) {
        e.target.classList.add('active');

        this.setState({
            [e.target.name]: e.target.value
        });

        this.showInputError(e.target.name);
    }

    showFormErrors() {
        const inputs = document.querySelectorAll('input');
        let isFormValid = true;

        inputs.forEach(input => {
            input.classList.add('active');

            const isInputValid = this.showInputError(input.name);

            if (!isInputValid) {
                isFormValid = false;
            }
        });

        return isFormValid;
    }

    showInputError(refName) {
        const validity = this.refs[refName].validity;
        const label = document.getElementById(`${refName}Label`).textContent;
        const error = document.getElementById(`${refName}Error`);
        const isEmail = refName.indexOf('email') !== -1;
        const isPassword = refName.indexOf('password') !== -1;
        const isPasswordConfirm = refName === 'passwordConfirm';

        if (isPasswordConfirm) {
            if (this.refs.password.value !== this.refs.passwordConfirm.value) {
                this.refs.passwordConfirm.setCustomValidity('Passwords do not match');
            } else {
                this.refs.passwordConfirm.setCustomValidity('');
            }
        }

        if (!validity.valid) {
            if (validity.valueMissing) {
                error.textContent = `${label} is a required field`;
            } else if (isEmail && validity.typeMismatch) {
                error.textContent = `${label} should be a valid email`;
            } else if (isEmail && validity.patternMismatch) {
                error.textContent = `${label} should be a valid email address`
            } else if (isPassword && validity.patternMismatch) {
                error.textContent = `${label} should be longer than 4 chars`;
            } else if (isPasswordConfirm && validity.customError) {
                error.textContent = 'passwords do not match';
            }
            return false;
        }

        error.textContent = '';
        return true;
    }

    render() {
        return (
            <div className="register-form">
                <form novalidate>
                    <div className="form-group">
                        <label id="usernameLabel">Full Name</label>
                        <input className="form-control"
                            type="text"
                            name="username"
                            ref="username"
                            value={this.state.username}
                            onChange={this.handleChange}
                            pattern="([A-Za-z]|\s){3,}"
                            required />
                        <div className="error" id="usernameError" />
                    </div>
                    <div className="form-group">
                        <label id="emailLabel">Email</label>
                        <input className="form-control"
                            type="email"
                            name="email"
                            ref="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                            required />
                        <div className="error" id="emailError" />
                    </div>
                    <div className="form-group">
                        <label id="passwordLabel">Password</label>
                        <input className="form-control"
                            type="password"
                            name="password"
                            ref="password"
                            value={this.state.password}
                            onChange={this.handleChange}
                            pattern=".{5,}"
                            required />
                        <div className="error" id="passwordError" />
                    </div>
                    <div className="form-group">
                        <label id="passwordConfirmLabel">Confirm Password</label>
                        <input className="form-control"
                            type="password"
                            name="passwordConfirm"
                            ref="passwordConfirm"
                            value={this.state.passwordConfirm}
                            onChange={this.handleChange}
                            required />
                        <div className="error" id="passwordConfirmError" />
                    </div>
                    <button className="btn btn-primary"
                        onClick={this.handleClick}>submit</button>
                </form>
            </div>
        );
    }
}

export default Register;