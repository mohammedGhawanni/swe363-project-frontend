import React from "react";

export default () =>
    <div className="NotFound">
        <h3>404: Sorry, page not found!</h3>
    </div>;