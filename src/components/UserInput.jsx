import React, { Component } from 'react';

class UserInput extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user: this.props.value,
    }
    this.handleChange = this.handleChange.bind(this);

  }
  handleChange(event) {
    var target = event.target;
    this.setState({ user: target.value });
  }

  render() {
    return (
      <div id="user-input-container">
        <input type="text" id="user-input" required value={this.state.user} onChange={this.handleChange} />
        <label htmlFor="user-input">User Name</label>
      </div>
    )
  }
}
export default UserInput