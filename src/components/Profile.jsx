import React, { Component } from 'react';
import {browserHistrory} from 'react-router';
import { Button, Row, Grid, Col} from 'react-bootstrap';
import { BarLoader } from 'react-spinners';
import axios from 'axios';
import '../styles/Profile.css';

export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //can use default values for style testing
      id: '',
      fullName: '',
      imgURL: '',
      title: '',
      department: '',
      bio: ''
    };

  }
  componentWillMount (){
    this.props.fetchData;
  }



  fetchData() {

    //if the path contains the id of a user 'id' will be assigned to it, otherwise the default will be to opend the user's profile
    // I didn't know how to get the logged in user's id so I typed '1234' for the moment
    // A third case must be add if an annonymous user try to open /profile/ 
    const id = this.props.params.id != undefined ? this.props.params.id : 1234;

    return axios.get(`http://172.20.10.13:3000//users/profile/${id}`,
      {
        headers: {
          "Authorization": "jwt " + sessionStorage.token
        }
      })
      .then(prof => prof.json())
      .then(res => {
        //Assuemed retured values from res
        this.setState({ id: res.id});
        this.setState({ fullName: res.name });
        this.setState({ imgURL: res.imgURL });
        this.setState({ title: res.title });
        this.setState({ department: res.department });
        this.setState({ bio: res.bio });
      });

  }

  render() {
    if(this.state.id == ''){
      return (
        // <div>Loading...</div>
        <div className='sweet-loading'>
          <BarLoader
            color={'#123abc'}
            loading={true}
          />
        </div>
      );
    }
    else{
      return (
        <div className="card">
          <img id="#avatar" src={this.state.imgURL} alt="profile pic" />
          <h1>{this.state.fullName}</h1>
          <p class="title">{this.state.title}</p>
          <p class="department">{this.state.department}</p>
          <p class="bio">{this.state.bio}</p>
          <p><button>Contact</button></p>
        </div>
      
    )
    }
  }
};
