import React, { Component } from 'react';
import {ReactDOM, ReactDOMServer} from 'react-dom';
import { Button, Row, Grid, Col } from 'react-bootstrap';
import '../styles/Search.css';
import axios from 'axios';
import Result from './Result';

var Link = require('react-router').Link;
var hashHistory = require('react-router').hashHistory;

class Search extends Component {

  constructor() {
    super();
    this.state = {
      searchTerm: '',
      resArray: '',
      showResults: false
    };
    this.handleSearch = this.handleSearch.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.setState({ searchTerm: e.target.value });
  }

  handleSearch(e) {
    e.preventDefault();
    if (this.refs.search.value != "") {
      {
        return axios.get(`http://localhost:3001/publications/search/${this.state.searchTerm}`,
          {
            headers: {
              "Authorization": "jwt " + sessionStorage.token
            }
          })
          .then(res => res.json())
          .then(publications => {
            console.log(publications);
            const markup = ReactDOMServer.renderToString(<Result props={publications} />);
              this.setState({ resArray: publications });
              this.context.router.push(`/results/${this.state.searchTerm}`)
              return { options: publications }
          });
      }
    }
  }
  componentWillMount() {
    this.setState({ isRecruiter: this.props.isRecruiter });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ isRecruiter: nextProps.isRecruiter });
  }

  //     function CreateTable() {
  //     let results = [];
  //     for (let i = 0; i < this.props.resArray.length; i++) {
  //         results.push(<Result props={this.props.resArray[i]} />)
  //     }
  //     return results;
  // }

  render() {
    const showResults = this.state.showResults ?
      (this.state.resArray.map(result => (
        <Result props={{ result }} />
      ))) : <div>empty for nonw</div>;

    return (
      <div>
        <form>
          <div className="search">
            <input onChange={this.handleChange} type="text" className="form-control navbar-search" ref="search" placeholder="Search a Publication" />
            <Button bsStyle="primary" onClick={this.handleSearch}>Search</Button>
          </div>
        </form>
        {showResults}
      </div>
    )
  }
}

export default Search