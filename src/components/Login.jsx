import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "../styles/Login.css";
import { Auth } from "aws-amplify";
import axios from "axios"
import Header from './Header';

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            password: ""
        };
    }

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = event => {
        event.preventDefault();
        //After submitting the credintials this will be executed
        var apiBaseURL = "http://localhost:3000/users/login";
        var self = this;
        var payload = {
            "password": this.state.password,
            "email": this.state.email
        }
        axios.post(apiBaseURL, payload,{
            headers: {
                'Content-Type': 'application/json'
            }
        }
        )
            .then((response) => {
                console.log(response);
                if (response.status == 200) {
                    console.log("Login Successful!");
                    sessionStorage.setItem('token', response.data.token);
                    this.props.history
                    this.props.userHasAuthenticated(true);
                }
                else if (response.status == 400) {
                    console.log("Login Failed!");
                }
                else if (response.status == 404) {
                    console.log("Login Not Found!");
                }
            });
    }

    render() {
        return (
            <div className="Login">
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="email" bsSize="large">
                        <ControlLabel>Email</ControlLabel>
                        <input className="form-control" onChange={this.handleChange} id="email" value={this.state.email} type="email" />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <input className="form-control" onChange={this.handleChange} id="password" value={this.state.password} type="password" />
                    </FormGroup>
                    <button className="btn btn-primary"
                        block
                        bsSize="large"
                        disabled={!this.validateForm()}
                        type="submit"
                    >
                        Login
                </button>
                </form>
            </div>
        );
    }
}