import React, { Component } from 'react';
import {Well} from 'react-bootstrap';

export default class componentName extends Component {
    constructor(props) {
        super(props);

    }
    render() {
        return (
            <div className="result">
                <Well bsSize="large">
                    <p>Pub. id: {this.props.id}</p>
                    <h3>Author: {this.props.author}</h3>
                    <p>Address: {this.props.location}</p>
                    <p>Book Title: {this.props.booktitle}</p>
                    <p>School: {this.props.school}</p>
                    <p>Publication Created at: {this.props.createdAt}</p>
                    <p>Publication Updated at: {this.props.updatedAt}</p>
                    <p>Profile id: {this.props.profileId}</p>
                </Well>
            </div>
        )
    }
};
