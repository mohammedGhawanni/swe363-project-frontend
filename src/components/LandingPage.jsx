import React, { Component } from 'react';
import '../styles/LandingPage.css';
import { Route } from 'react-router-dom';
import { Grid, Row, Image, Col } from 'react-bootstrap';
import Header from './Header';
import Search from './Search';

class LandingPage extends Component {
  render() {
    return (
      <div className="LandingPage">
        <Header />
        <div className="searchContainer">
        <Search />
        </div>
      </div>
    );
  }
}

export default LandingPage;
